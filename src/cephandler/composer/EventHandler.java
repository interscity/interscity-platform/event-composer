package cephandler.composer;

import cephandler.utils.InterscityEventType;
import cephandler.exception.InvalidParameterException;
import cephandler.exception.MalformatedSchemaException;
import cephandler.helper.CepEventHelper;
import cephandler.composer.helper.InterscityEventHelper;
import com.rabbitmq.client.*;
import org.apache.avro.Schema;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;

public class EventHandler{

    private String QUEUE_NAME = "";
    private final String EXCHANGE_NAME = "data_stream";
    private final String ROUTING_KEY = "#";

    private final String CEP_EXCHANGE_NAME = "composer_exchange";
    private final String CEP_ROUTING_KEY = "composer_send_tag";
    private String CEP_QUEUE_NAME = "";

    private ConnectionFactory connectionFactory;
    private Connection connection;
    private Channel channel;
    private Channel cepChannel;
    private ExecutorService executor;
    private CepEventHelper cepEventHelper;



    public EventHandler(){
        cepEventHelper = new CepEventHelper();
        connectionFactory = new ConnectionFactory();
        connectionFactory.setHost(System.getenv("RABBITMQ_HOST"));
//        connectionFactory.setPort(new Integer(System.getenv("RABBITMQ_PORT")));
        connectionFactory.setUsername(System.getenv("RABBITMQ_USERNAME"));
        connectionFactory.setPassword(System.getenv("RABBITMQ_PASSWORD"));


        this.executor = Executors.newCachedThreadPool();
    }

    private void startConnection() throws IOException, TimeoutException {
        cepEventHelper.startConnection();

        connection = connectionFactory.newConnection();

        /*rabbit connection to Interscity*/
        channel = connection.createChannel();
        channel.exchangeDeclare(EXCHANGE_NAME, "topic");
        QUEUE_NAME = channel.queueDeclare().getQueue();
        channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, ROUTING_KEY);

        /*rabbit connection to cep-cataloger*/
//        cepChannel = connection.createChannel();
//        cepChannel.exchangeDeclare(CEP_EXCHANGE_NAME,"topic");
//        CEP_QUEUE_NAME = cepChannel.queueDeclare().getQueue();
//        cepChannel.queueBind(CEP_QUEUE_NAME,CEP_EXCHANGE_NAME,CEP_ROUTING_KEY);
    }


    private Consumer defineConsumer(InterscityEventType staticEventType){
        return new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
                System.out.println("\n\nData Stream Captured: "+ new String(body));
                try {
                    if(staticEventType == null){
                        InterscityEventType arrivedEventType  = searchEventType(envelope.getRoutingKey(), body);
                        onNewMessageArrived(arrivedEventType,envelope,body);
                    } else {
                        onNewMessageArrived(staticEventType, envelope,body);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
    }


    /**
     * {
     *     r_uui:
     *     capability:
     *     seng_tag:
     * }
     *
     * @return
     */
//    private Consumer defineCepConsumer(){
//        return new DefaultConsumer(cepChannel) {
//            @Override
//            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
//                System.out.println("\n\n Topic Set SendTag Received: "+new String(body));
//                InterscityEventHelper.getInstance().setSendTag(body);
//            }
//        };
//    }

    private InterscityEventType searchEventType(String routingKey, byte[] messageBody) throws IOException {
        InterscityEventType interscityEventType = null;

        String[] stringParts = routingKey.split("\\.");
        String rUuid = stringParts[0];
        String capability = stringParts[1];

        interscityEventType = InterscityEventHelper.getInstance().takeAndPersistInterscityEventType(rUuid,capability, messageBody);

        return interscityEventType;
    }

    private void onNewMessageArrived(InterscityEventType eventType, Envelope envelope, byte[] body) {
        /*processing the routing key*/
        String routingKey = envelope.getRoutingKey();
        System.out.println("Routing Key: "+ routingKey);

        String[] stringParts = routingKey.split("\\.");
        String rUuid = stringParts[0];

        boolean sendTag = InterscityEventHelper.getInstance().canSendEvent(eventType);
        if(sendTag){
            /*processing the body message*/
            try {
                Schema schema = eventType.getAvroSchema();
                System.out.println("Schema: "+ schema);

                List eventValues = new ArrayList(eventType.getEventFieldsValues());

                cepEventHelper.sendCepEvent(rUuid, eventType.getEventName(), eventValues, eventType.getEventFields() ,schema);

            } catch (MalformatedSchemaException | InvalidParameterException e) {
                e.printStackTrace();
            }
        }

    }


    /**
     * Start processing the new occurrences of a given Interscity Event Type
     *
     * @param eventType
     * @throws IOException
     * @throws TimeoutException
     */
    public void staticEventProcessing(InterscityEventType eventType) throws IOException, TimeoutException {
        if(this.connection == null || !this.connection.isOpen()){
            this.startConnection();
        }

        while(true){
            channel.basicConsume(QUEUE_NAME,true,defineConsumer(eventType));
        }
    }



    /**
     * Automatic detect an Interscity Event Type for each Interscity Message
     */
    public void smartEventProcessing() throws IOException, TimeoutException {
        System.out.println("Event-Composer started...");
        if(this.connection == null || !this.connection.isOpen()){
            this.startConnection();
        }
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                while(true){
//                    try {
//                        cepChannel.basicConsume(CEP_QUEUE_NAME,true,defineCepConsumer());
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        }).start();

        while(true){
            channel.basicConsume(QUEUE_NAME,true,defineConsumer(null));
        }
    }

}

