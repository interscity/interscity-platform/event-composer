package cephandler.composer;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class EventComposer {
    public static void main(String[] args){
        EventComposer eventComposer = new EventComposer();

        eventComposer.startSmartEventComposer();

    }



    public void startSmartEventComposer(){
        EventHandler eventHandler = new EventHandler();

        try {
            eventHandler.smartEventProcessing();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }
}
