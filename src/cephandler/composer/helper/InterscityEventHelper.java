package cephandler.composer.helper;

import cephandler.utils.GenericInterscityEvent;
import cephandler.helper.LocalPersistenceMappingHelper;
import cephandler.utils.InterscityEventType;
import cephandler.utils.Types;
import cephandler.exception.TypeNotSupportedException;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;


/**
 * A singleton to handle the Interscity interaction.
 *
 */
public class InterscityEventHelper {
    public static final String RESOURCE_UUID_ATTRIBUTE = "r_uuid";
    public static final String CAPABILITY_ATTRIBUTE = "capability";
    public static final String SEND_TAG_ATTRIBUTE = "send_tag";


    private static InterscityEventHelper instance;
    private String EVENT_PREFIX = System.getenv("EVENT_PREFIX");
    private String RESOURCE_CATALOGER_URL = System.getenv("RESOURCE_CATALOGER_URL");
//    private String INTERSCITY_HOST = System.getenv("INTERSCITY_HOST");
//    private String INTERSCITY_PORT = System.getenv("INTERSCITY_PORT");
    private String REDIS_HOST = System.getenv("REDIS_HOST");

    private LocalPersistenceMappingHelper rscUuidToInterEventType;

    private InterscityEventHelper(){
        this.rscUuidToInterEventType = new LocalPersistenceMappingHelper(REDIS_HOST);
    }


    /**
     * Take the message fields from the body message
     *
     * @param bodyMessage
     * @return
     */
    public List<String> takeMessageFields(byte[] bodyMessage){
        return this.takeMessageFields(new String(bodyMessage));
    }

    /**
     * Take the message fields from the json message
     *
     * @param jsonMessage
     * @return
     */
    public List<String> takeMessageFields(String jsonMessage){
        List<String> fields = new ArrayList<String>();

        JSONObject jsonObject = new JSONObject(jsonMessage);
        Iterator<String> keys = jsonObject.keys();

        while (keys.hasNext())
            fields.add(keys.next());

        return fields;
    }

    /**
     * Take the resource informations from Interscity plattaform and generate the InterscityEventType object.
     *
     * @param resourceUuid
     * @param capability
     * @param bodyMessage
     * @return
     * @throws IOException
     */
    public InterscityEventType takeAndPersistInterscityEventType(String resourceUuid, String capability, byte[] bodyMessage) throws IOException {
        String hashKey = resourceUuid+"."+capability;
        String eventName = new String(EVENT_PREFIX+"_"+resourceUuid+"_"+capability).replaceAll("-","");
        InterscityEventType interscityEventType = null;
        String jsonResourceData;

        try {
            System.out.println("Check HashKey: "+hashKey);
            if (rscUuidToInterEventType.containsKey(hashKey)) {
                System.out.println("Contains HashKey: "+hashKey);
                interscityEventType = rscUuidToInterEventType.get(hashKey);
                jsonResourceData = interscityEventType.getJsonResourceData();

            } else {
                jsonResourceData = takeInterscityResourceJsonData(resourceUuid);
            }

            System.out.println("Prepare the event: ");
            String jsonResInternalData = getResourceInternalData(jsonResourceData);

            List<String> resFields = this.takeMessageFields(jsonResInternalData);
            List<String> capabilityFields = this.takeMessageFields(bodyMessage);

            List resourceValues = this.takeFieldsValues(resFields, jsonResInternalData);
            List capabilityValues = this.takeFieldsValues(capabilityFields,new String(bodyMessage));

            List fieldsType = new ArrayList();

            for (int i = 0; i < resFields.size(); i++) {
                Object value = resourceValues.get(i);
                fieldsType.add(Types.get(value.getClass().toString()));
            }

            for (int i = 0; i < capabilityFields.size(); i++) {
                resFields.add(capabilityFields.get(i));
                Object value = capabilityValues.get(i);
                fieldsType.add(Types.get(value.getClass().toString()));
            }


            if(interscityEventType == null) {
                String cepEventUuid = UUID.randomUUID().toString();

                interscityEventType = new GenericInterscityEvent(cepEventUuid, eventName, resourceUuid, capability, resFields, fieldsType, jsonResourceData);
            }
            for(int i = 0; i < resourceValues.size(); i++){
                String field = resFields.get(i);
                Object value = resourceValues.get(i);
                interscityEventType.addFieldValue(field, correctValue(value));
            }
            for(int i = 0; i < capabilityValues.size(); i++){
                String field = capabilityFields.get(i);
                Object value = capabilityValues.get(i);

                interscityEventType.addFieldValue(field, correctValue(value));
            }


            if (!rscUuidToInterEventType.containsKey(hashKey)) {
                /*processing the message persistence*/

                PersistenceHelper persistenceHelper = new PersistenceHelper(REDIS_HOST);
                persistenceHelper.saveNewEvent(interscityEventType);

                rscUuidToInterEventType.put(hashKey, interscityEventType);
            }

            return interscityEventType;
        } catch (TypeNotSupportedException | ClassNotFoundException e){
            throw new IOException(e);
        }
    }

    /**
     * A recursive method to take the serializable values from the JsonObjects
     *
     * @param value
     * @return
     */
    private Object correctValue(Object value) {
        Object newValue = null;
        if(value == org.json.JSONObject.NULL){
            newValue = null;
        } else if( value instanceof JSONArray){
            newValue = ((JSONArray) value).toString();
        } else {
            newValue = value;
        }
        return  newValue;
    }

    /**
     *
     * @param resourceFields
     * @param json
     * @return
     */
    private List takeFieldsValues(List<String> resourceFields, String json) {
        JSONObject jsonObject = new JSONObject(json);
        List<Object> rscValues = new ArrayList<>();
        Iterator<String> interator = resourceFields.iterator();
        while (interator.hasNext()) {
            rscValues.add(jsonObject.get(interator.next()));
        }
        return rscValues;
    }

    private String getResourceInternalData(String jsonResourceData) {

        JSONObject jsonObject = new JSONObject(jsonResourceData);
        JSONObject intJsonObject = (JSONObject) jsonObject.get("data");

        return intJsonObject.toString();
    }


    /**
     * Return the Resource Data From Interscity Plattaform for the given resource uuid
     *
     * @param resourceUuid
     * @return
     * @throws IOException
     */
    private String takeInterscityResourceJsonData(String resourceUuid) throws IOException {
//        System.out.println("Executing request to: "+"http://"+INTERSCITY_HOST+":"+INTERSCITY_PORT+"/catalog/resources/"+resourceUuid);
        System.out.println("Executing request to: "+RESOURCE_CATALOGER_URL+"/"+resourceUuid);
        try {
            URL url = new URL(RESOURCE_CATALOGER_URL+"/"+resourceUuid);//your url i.e fetch data from .
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", "application/json");

            if (conn.getResponseCode() != 200 && conn.getResponseCode() != 201) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }

            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output = "";
            String result = "";
            while ((output = br.readLine()) != null) {
                result = result.concat(output);
            }
            conn.disconnect();

            return result;
        }catch (IOException io){
            throw new IOException("Can not connect to Intercity Plataform for the host "+RESOURCE_CATALOGER_URL+"/"+resourceUuid,io);
        }
    }


    public boolean canSendEvent(InterscityEventType interscityEventType){
        return rscUuidToInterEventType.canSendEvent(interscityEventType.getCepEventUuid());
    }

    /**
     * Used to set the send tag for an event.
     *
     * @param messageBody
     */
//    public void setSendTag(byte[] messageBody) {
//        String jsonData = new String(messageBody);
//        JSONObject jsonObject = new JSONObject(jsonData);
//
//        String rUUID = jsonObject.getString(RESOURCE_UUID_ATTRIBUTE);
//        String capability = jsonObject.getString(CAPABILITY_ATTRIBUTE);
//        Boolean sendTag = jsonObject.getBoolean(SEND_TAG_ATTRIBUTE);
//
//        try {
//            InterscityEventType eventType = rscUuidToInterEventType.get(rUUID+"."+capability);
//
//            rscUuidToInterEventType.setSendEventTag(eventType.getCepEventUuid(), sendTag);
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//    }

    /**
     * Take the resource informations from Interscity plattaform and generate the InterscityEventType object.
     *
     * @param resourceUuid
     * @param capability
     * @return
     * @throws IOException
     */
    public InterscityEventType searchForInterscityEventType(String resourceUuid, String capability) throws IOException {
        String hashKey = resourceUuid+"."+capability;
        String eventName = EVENT_PREFIX+"_"+capability;
        InterscityEventType interscityEventType = null;
        String jsonResourceData;

        try {
            interscityEventType = rscUuidToInterEventType.get(hashKey);

        } catch (ClassNotFoundException e) {
            throw new IOException(e);
        }

        return interscityEventType;

    }

    public static InterscityEventHelper getInstance(){
        if(instance == null){
            instance = new InterscityEventHelper();
        }

        return instance;
    }


}
